# privacy-preserving-anti-pandemic-systems

Exploring the possibilities of anti-pandemic systems (track-and-trace,
immunity certificates, ...) which do not surrender privacy.

## Immunity Certificates

### References

* [Is an 'immunity certificate' the way to get out of coronavirus lockdown?](https://www.cnn.com/2020/04/03/health/immunity-passport-coronavirus-lockdown-intl/index.html)

* [Are coronavirus immunity certificates really such a good idea?](https://theweek.com/articles/906254/are-coronavirus-immunity-certificates-really-such-good-idea)

### Documents

* [Privacy Preserving Immunity Certificate System Requirements](https://cryptpad.fr/pad/#/2/pad/view/ptTuoZ7FTqg+NgsPIoMs1P5aGByybJg8PTPku5tmoE4/embed/)

## Track and Trace

### References

* [COVID-19’s New Reality—These Smartphone Apps Track Infected People Nearby](https://www.forbes.com/sites/zakdoffman/2020/04/07/covid-19s-new-normal-yes-your-phone-will-track-infected-people-nearby/#270039477f0d)

* [‘Track and trace’ is key to containing COVID-19: how privacy can be protected](https://theconversation.com/track-and-trace-is-key-to-containing-covid-19-how-privacy-can-be-protected-134369)

* [Clever Cryptography Could Protect Privacy in Covid-19 Contact-Tracing Apps](https://www.wired.com/story/covid-19-contact-tracing-apps-cryptography/)

### Documents

* [Privacy Preserving Track and Trace System Requirements](https://cryptpad.fr/pad/#/2/pad/view/holO4-vPmcCVYf4IpmdrrwR7rWg9UOyCX889MRSRoHI/embed/)
